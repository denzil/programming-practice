-- startup init function
function love.load()
	love.graphics.setMode( 800, 600, false, true)
	
	love.graphics.setColor( 0, 0, 0, 255)
	love.graphics.setBackgroundColor( 255, 255, 255)
	
	x_center = love.graphics.getWidth()/2
	y_center = love.graphics.getHeight()/2	
end

-- handle keyboard input
function love.keypressed( key, unicode)
	if key == "escape" then
		-- "q" is builtin quit event
		love.event.push( "q")
	end
end

function love.draw()
	love.graphics.print( "Press \"Esc\" to quit.", 01, 0)
	draw_clock()
end

function draw_clock()
	cdate = os.date( "*t")
	-- important fields: hour, min, sec
	hr = cdate.hour
	mins = cdate.min
	sec = cdate.sec
	output_string = string.format( "It's: %u:%02u:%02u", hr, mins, sec)
	love.graphics.print( output_string, 01, 15)
	
	-- calculate clock hands angles
	hr_dx, hr_dy = calculate_hand_angles( ( hr+mins/60), 12) -- hours
	min_dx, min_dy = calculate_hand_angles ( ( mins+sec/60), 60) -- minutes
	sec_dx, sec_dy = calculate_hand_angles( sec, 60) -- seconds
	
	-- draw clock hands
	draw_hand( x_center, y_center, hr_dx, hr_dy, 70)
	draw_hand( x_center, y_center, min_dx, min_dy, 90)
	love.graphics.setColor( 255, 0, 0, 255)
	draw_hand( x_center, y_center, sec_dx, sec_dy, 90)
	love.graphics.setColor( 0, 0, 0, 255)	
	
	-- draw other parts of clock
	-- I do it after the hands so the second clock isn't drawn over the central circle
	love.graphics.circle( "fill", x_center, y_center, 6)
	
	for i=1,12 do
		dx, dy = calculate_hand_angles( i, 12)
		
		love.graphics.line( x_center+dx*110, y_center+dy*110,
								x_center+dx*130, y_center+dy*130)
								
		-- for centring the digits
		text_width = love.graphics.getFont():getWidth( tostring( i))
		text_height = love.graphics.getFont():getHeight( tostring( i))
		-- print clock digits
		love.graphics.print( i, x_center+dx*140-text_width/2, y_center+dy*140-text_height/2)
	end
end

function calculate_hand_angles( value, max)
	dx = math.cos( 2*math.pi/max*( value-(max/4)))
	dy = math.sin( 2*math.pi/max*( value-(max/4)))
	return dx, dy
end

function draw_hand( x_center, y_center, dx, dy, length)
	love.graphics.line( x_center, y_center,
							x_center+dx*length, y_center+dy*length)
end