﻿Simple analog clock
I created it as a part of my learning of LÖVE framework.

Requires:
LÖVE framework: http://love2d.org/

Use - Windows
- put the analog_clock folder into the love2d folder
- from the command line run:
love.exe analog_clock

Use - Linux ( not tested yet)
- put the analog_clock directory into the love2d directory
- run in the terminal:
love analog_clock

Limitations:
- I don't test if init ran correctly and generaly don't do any checks yet.
- Redraws whole screen for each frame. This is quite inefficient, but it's not really a problem for such simple demo. Simple fix would be to check if time changed and redraw only then.
- Hand showing seconds jumps from one position to the next one, I consider this a correct behavior.
