log = {
	hide	= -1,
	all		= 0,
	debug	= 1,
	gui		= 2,
	normal	= 3,
	error	= 4,
	level	= 3,
	}
local count = 1

function log:add( data, level )
	level = level or 100
	--io.write( data.." (log lvl: "..tostring( level )..")\n" )
	io.flush()
	if level >= self.level then
		--table.insert( self, tostring( count )..": "..tostring( data ))
		table.insert( self, tostring( data ))
		count = count + 1
		io.write( data.." (log lvl: "..tostring( level )..")\n" )
	end
end


function log:get( number )
	if number >= #log then
		return table
	end
	rt = {}
	for i = #log - number, #log do
		table.insert( rt, log[ i ])
	end
	return rt
end



return log