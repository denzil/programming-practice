local class = require 'middleclass'


-------------------------------------------------------------------

local events = {
	click = "click",
	tick  = "tick",
	hover = "hover",
	unhover = "unhover",
	is_enabled = "is_enabled",
	item_sizer = "item_sizer",
}

-------------------------------------------------------------------
Position = class( "Position" )
function Position:initialize( x, y )
	self.x = x or 0
	self.y = y or 0
end

function Position:set( x, y )
	self.x = x or self.x
	self.y = y or self.y
	--table.insert( logger, "pos x: "..tostring(x)..", y: "..tostring(y))
end

function Position:get()
	return self.x, self.y
end


-------------------------------------------------------------------
-------------------------------------------------------------------
-- template for other UI classes
local UIBase = class( "UIBase" )
function UIBase:initialize()
	self.pos   		= Position:new()
	--self.pos   		= { x=0, y=0 }
	self.size  		= { x=0, y=0 }
	self.items 		= {}
	self.item_list	= {}
	self.handlers 	= {}
	self.handle 	= nil
	self.font		= love.graphics.getFont()
	self.enabled	= true
	self.parent		= nil
	
	self.cfg		= {
		padding		= 2, -- space for items inside the element
		spacing		= 1, -- space between multiple elements stored
		hidden		= false,
		enabled		= true,
		hovered		= false,
		selected	= false,
	}
end

function UIBase:get_size()
	return self.size.x, self.size.y
end

function UIBase:event( evt )
	if self.handlers[ evt ] then
		self.handle = self.handlers[ evt ]
		return self:handle( evt )
	end
	for id, item in ipairs( self.items ) do
		local handler = item.handlers[ evt ]
		--table.insert( logger, "event: "..tostring(evt)..", handler: "..tostring( handler ))

		if handler then
			item.handle = handler
			item:handle( evt )
		else
			item:event( evt )
		end
	end
end

function UIBase:is_hovered()
	local mx, my = love.mouse.getPosition()
	local xs, ys = self.size.x, self.size.y
	if mx > self.pos.x and mx < self.pos.x+xs and my > self.pos.y and my < self.pos.y+ys then
		if not self.cfg.hovered then
			self.cfg.hovered = true
			self:event( events.hover )
			--log:add( "hover event sent" )
		end
	else
		if self.cfg.hovered then
			self.cfg.hovered = false
			self:event( events.unhover )
		end
	end
	return self.cfg.hovered
end

function UIBase:add( item )
	--log:add( "Add item: "..tostring( item )..", table: "..tostring( self.items ))
	item.parent = self
	table.insert( self.items, item )
	self.items[ item ] = #self.items or true
	--log:add( tostring( item ).." added to list: "..tostring( self.items[ item ]))
	--log:add( "add "..tostring(item).." at pos: "..tostring( self.pos.x )..", "..tostring( self.pos.x + self.size.x ))
	item:set_pos( self.pos.x + self.cfg.padding/2, self.pos.y + self.cfg.padding/2 )
	self:update_size()
	if self.parent then
		self.parent:update_size()
	end
end

function UIBase:remove( item )
	log:add( "remove element - start: "..tostring( item ))
	log:add( "status: "..tostring( self.items[ item ]))
	for id, elem in ipairs( self.items ) do
		log:add( tostring( id ).." "..tostring( elem ).." = "..tostring( item == elem) )
	end
	
	log:add( "remove list end" )
	if self.items[ item ] ~= nil then
		log:add( "remove element - found" )
		table.remove( self.items, self.items[ item ])
		self.items[ item ] = nil
		self:update_size()
		if self.parent then
			self.parent:update_size()
		end
	end
end

function UIBase:set_pos( x, y )
	local mx, my = x - self.pos.x, y - self.pos.y
	self.pos.x = x
	self.pos.y = y
	--log:add( "set_pos - self: "..tostring( self ).." at pos: "..tostring( self.pos.x )..", "..tostring( self.pos.y ))
	--log:add( "set_pos - items: "..tostring( #self.items ))
	for id, item in ipairs( self.items ) do
		item:set_pos( item.pos.x + mx, item.pos.y + my )
		--log:add( "set_pos - item: "..tostring( item ).." at pos: "..tostring( item.pos.x )..", "..tostring( item.pos.y ))
	end
end

function UIBase:update_size()
	-- pass
end

function UIBase:set_enabled( state )
	self.enabled = state
	for id, item in ipairs( self.items ) do
		item:set_enabled( state )
	end
end

-------------------------------------------------------------------
-------------------------------------------------------------------
local Button = class( "Button", UIBase )
function Button:initialize( content )
	UIBase.initialize( self )
	self.content 	= content
	self:add( content )
	
	local cx, cy = content:get_size()
	self.size.x = cx + 5
	self.size.y = cy + 5
	--log:add( "button size: "..tostring(self.size.x)..", "..tostring(self.size.y))
	self.cfg.show_frame = true
end

function Button:draw( xpos, ypos)
	local xpos = xpos or self.pos.x
	local ypos = ypos or self.pos.y
	
	self:is_hovered()
	if self.handlers[ events.is_enabled ] then
		self.cfg.enabled = self:event( events.is_enabled )
		self:set_enabled( self.cfg.enabled )
		--log:add( "enabled: "..tostring( enabled ))
	end
	
	if self.cfg.hovered then
		love.graphics.setColor( 64, 64, 64, 255)
		love.graphics.rectangle( "fill", self.pos.x, self.pos.y, self.size.x, self.size.y)
		love.graphics.setColor( 255, 255, 255, 255)
	end
	
	--self:get_size()
	--self.content:set_pos( self.pos.x+2, self.pos.y+2)
	
	--love.graphics.print( self.text, xpos+2, ypos+2)
	if self.cfg.show_frame then
		love.graphics.rectangle( "line", xpos, ypos, self.size.x-1, self.size.y-1)
	end
	self.content:draw()
end


-------------------------------------------------------------------
local Text = class( "Text", UIBase )
function Text:initialize( text )
	--log:add( "create text "..tostring( self ))
	UIBase.initialize( self )
	self.text = tostring( text )
	self:get_size()
end

function Text:draw()
	-- update content if neede
	if  self.update then
		self:update()
	end
	
	self:is_hovered()
	
	if self.cfg.hovered and false then
		love.graphics.setColor( 64, 64, 64, 255)
		love.graphics.rectangle( "fill", self.pos.x, self.pos.y, self.size.x, self.size.y)
		love.graphics.setColor( 255, 255, 255, 255)
	end

	local xpos = self.pos.x
	local ypos = self.pos.y
	
	if not self.enabled then
		love.graphics.setColor( 128, 128, 128, 255)
	end
	love.graphics.print( self.text, xpos, ypos )
	love.graphics.setColor( 255, 255, 255, 255)
	--table.insert( logger, "print "..self.text.." at "..tostring(xpos)..", "..tostring(ypos))
end

function Text:get_size()
	local height = self.font:getHeight()
	local width  = self.font:getWidth( self.text )
	
	self.size.x, self.size.y = width, height
	
	log:add( "text "..self.text.." - h: "..tostring(height)..", w: "..tostring(width), log.debug)
	return self.size.x, self.size.y
end
	
function Text:set( text )
	--table.insert( logger, "button text update: "..text)
	self.text = text
	self:get_size()
end


-------------------------------------------------------------------
local VBox = class( "Vbox", UIBase )
function VBox:initialize()
	UIBase.initialize( self )
end

function VBox:update_size()
	--table.insert( logger, "vbox size before: "..tostring( self.size.x )..", "..tostring( self.size.y ))
	local height = 0
	local width  = 0
	
	if self.handlers[ events.item_sizer ] then
		self:event( events.item_sizer )
	end
	
	for id, item in ipairs( self.items ) do
		item:set_pos( self.pos.x + self.cfg.padding, self.pos.y + self.cfg.padding + height + self.cfg.spacing )
		item:update_size()
		local h, w = item.size.y, item.size.x
		height = height + h + self.cfg.padding + self.cfg.spacing
		if w+self.cfg.padding>width then
			width = w+self.cfg.padding
		end
	end
	
	self.size.y = height
	self.size.x  = width
	--table.insert( logger, "vbox size: "..tostring( self.size.x )..", "..tostring( self.size.y ))
end

function VBox:draw( xpos, ypos )
	local x = xpos or self.pos.x
	local y = ypos or self.pos.y
	
	for id, item in ipairs( self.items ) do
		item:draw()
	end
end

function VBox:add( item )
	item.parent = self
	table.insert( self.items, item )
	self.items[ item ] = #self.items or true
	--log:add( "box add "..tostring( item).." at pos: "..tostring( self.pos.x )..", "..tostring( self.pos.x + self.size.x ))
	--log:add( "box add - items: "..tostring( #self.items))
	item:set_pos( self.pos.x + self.cfg.padding, self.pos.y + self.cfg.padding + self.size.y + self.cfg.spacing )
	if self.parent then
		-- find topmost holder and run update size+positions for all children
		parent = self.parent
		while parent.parent do
			parent = parent.parent
		end
		parent:update_size()
	else
		self:update_size()
	end
	--log:add( "vbox size: "..tostring( self.size.x )..", "..tostring( self.size.y ))
	--log:add( "box add - f items: "..tostring( #self.items))
end
	
-------------------------------------------------------------------
local HBox = class( "Hbox", UIBase )
function HBox:initialize()
	UIBase.initialize( self )
end

function HBox:update_size()
	local height = 0
	local width  = 0
	
	if self.handlers[ events.item_sizer ] then
		self:event( events.item_sizer )
	end
	
	for id, item in ipairs( self.items ) do
		-- update positions of held items too (could change)
		item:set_pos( self.pos.x + self.cfg.padding + width + self.cfg.spacing, self.pos.y + self.cfg.padding )
		
		item:update_size()
		local xs, ys = item.size.x, item.size.y
		width = width + xs + self.cfg.padding + self.cfg.spacing
		if ys+self.cfg.padding>height then
			height = ys+self.cfg.padding
		end
	end
	
	self.size.y = height
	self.size.x  = width
	--table.insert( logger, "box h: "..tostring(height)..", w: "..tostring(width))
end

function HBox:draw( xpos, ypos )
	local x = xpos or self.pos.x
	local y = ypos or self.pos.y
	
	for id, item in ipairs( self.items ) do
		item:draw()
	end
end

function HBox:add( item )
	item.parent = self
	table.insert( self.items, item )
	self.items[ item ] = #self.items or true
	--log:add( "box add "..tostring( item).." at pos: "..tostring( self.pos.x )..", "..tostring( self.pos.x + self.size.x ))
	--log:add( "box add - items: "..tostring( #self.items))
	item:set_pos( self.pos.x + self.cfg.padding + self.size.x + self.cfg.spacing, self.pos.y + self.cfg.padding )
	
	--log:add( "box add - f items: "..tostring( #self.items))
	if self.parent then
		-- find topmost holder and run update size+positions for all children
		parent = self.parent
		while parent.parent do
			parent = parent.parent
		end
		parent:update_size()
	else
		self:update_size()
	end
end
	

-------------------------------------------------------------------
local Bar = class( "Bar", UIBase )
function Bar:initialize( status, length )
	UIBase.initialize( self )
	--self.pos = Position:new()
	self.status = status
	self.size.x = length
	self.size.y = 1
	
	--log:add( "create bar "..tostring( self.pos ))
end
	
function UIBase:draw()
	if self.status ~= 0 then
		local drawlen = self.size.x * (self.status/100)
		love.graphics.line( self.pos.x, self.pos.y, self.pos.x + drawlen, self.pos.y )
	end
end


-------------------------------------------------------------------
local TabButton = class( "TabButton", Button )
function TabButton:initialize( txt )
	self.btxt = Text:new( txt )
	--log:add( "tabbuton "..tostring( self.btxt ))
	self.bbar = Bar:new( 0, self.btxt.size.x )
	--log:add( "size of bar: "..tostring( self.text.size.x ))
	self.bbox = VBox:new()
	self.bbox:add( self.btxt )
	self.bbox:add( self.bbar )
--	self:add( self.box )
	Button.initialize( self, self.bbox )
	self.cfg.show_frame = false
	self.handlers[ events.click ] = function( )
		--log:add( "TabButton clicked" )
		if self.cfg.selected then
			self.cfg.selected = false
			self.bbar.status = 0
		else
			self.cfg.selected = true
			self.bbar.status = 100
		end
	end
	--log:add( "tab button size: "..tostring( self.size.x )..", "..tostring( self.size.y ))
end
-------------------------------------------------------------------

local TabBox = class( "TabBox", VBox )

function TabBox:initialize()
	VBox.initialize( self )
	--self.vbox = VBox:new()
	self.buttons = HBox:new()
	--local txt = Text:new( "Test tbox" )
	--self.vbox:add( txt )
	self:add( self.buttons )
	--self:add( self.vbox )
	--log:add( "buttons parent: "..tostring( self.buttons.parent ))
end

function TabBox:add_tab( txt, content )
	local tbutton = TabButton:new( txt )
	local id = #self.buttons.items + 1
	--log:add( "Add button #"..tostring( id ))
	--tbutton.content = content
	tbutton.handlers[ events.click ] = function ()
		if not tbutton.cfg.hovered then
			return
		end
		for id, button in ipairs( self.buttons.items) do
			button.cfg.selected = false
			button.bbar.status = 0
		end
		tbutton.cfg.selected = true
		tbutton.bbar.status = 100
		--log:add( "clicked button: "..tostring( tbutton.btxt ))
		self.items[ 2 ] = nil
		self:add( content )
		--log:add( "tab box size: "..tostring( self.size.x )..", "..tostring( self.size.y ))
		--log:add( "tab button box size: "..tostring( self.buttons.size.x )..", "..tostring( self.buttons.size.y ))
	end
	
 	self.buttons:add( tbutton )
	if #self.buttons.items == 1 then
		tbutton.cfg.selected = true
		tbutton.bbar.status = 100
		self:add( content )
	end
	--og:add( "tab box size: "..tostring( self.size.x )..", "..tostring( self.size.y ))
end
		
function TabBox:draw()
	--log:add( "tbox draw" )
	for id, item in ipairs( self.items ) do
		--log:add( "tbox draw "..tostring( item ))
		item:draw()
	end
end
	
-------------------------------------------------------------------
function same_x_sizer( self , evt, new_x )
	if not new_x then 
		local max_x = 0
		for id, item in ipairs( self.items ) do
			x_size = item.size.x
			if x_size > max_x then
				max_x = x_size
			end
		end
		new_x = max_x
	end
	
	for id, item in ipairs( self.items ) do
		item.size.x = new_x
		if #item.items~=0 then
			same_x_sizer( item, events.item_sizer, new_x-self.cfg.padding )
		end
	end
end

-------------------------------------------------------------------
-------------------------------------------------------------------
-------------------------------------------------------------------
local ui 	= {}
ui.events	= events

ui.button 	= Button
ui.text 	= Text
--ui.position = Position
ui.vbox 	= VBox
ui.hbox 	= HBox
ui.bar		= Bar
ui.tbutton	= TabButton
ui.tbox		= TabBox
return ui