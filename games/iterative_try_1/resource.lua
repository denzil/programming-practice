local class 		= require 'middleclass'
local ui 			= require "ui"

------------------------------------------------------------------
Resource = class( "Resource" )
function Resource:initialize( name, value, max )
	self.name	= name
	self.value	= value or 0
	self.max	= max or nil
	
	local ui_fin = ""
	if self.max then
		ui_fin = "/"..tostring( self.max )
	end
	self.ui 	= ui.text:new( self.name..": "..tostring( self.value )..ui_fin)
end

function Resource:update()
	-- unused
end

function Resource:update_ui()
	local ui_fin = ""
	if self.max then
		ui_fin = "/"..tostring( self.max )
	end
	self.ui:set( self.name..": "..string.format( "%u", self.value )..ui_fin )
end


function Resource:add( value )
	if value then
		self.value = self.value + value
		if self.max and self.value > self.max then
			self.value = self.max
		end
	end
	self:update_ui()
end

function Resource:is_max()
	return self.value >= self.max
end

function Resource:increase_max( val )
	if self.max then
		self.max = self.max + val
		self:update_ui()
	else
		log:add( "Trying to increase maximum from unset value for: "..self.name )
	end
end

function Resource:get_available()
	return self.value
end

function Resource:get_max()
	return self.max
end

function Resource:get( val )
	self.value = self.value - val
	self:update_ui()
end

		
--function Resource