local ui	= require "ui"
local class	= require "middleclass"
local grid	= require "grid"


---------------------
local resources = vars.resources

---------------------
local destroy_building = ui.button:new( ui.text:new( "Destroy building" ))
destroy_building.hint = "Demolishes local building, if there is any"

destroy_building.handlers[ ui.events.is_enabled ] = function( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	if tile.buildings.remove_needs ~= nil then
		for id, item in pairs( tile.buildings.remove_needs ) do
			local resource, need_val = unpack( item )
			if vars.resources[ resource ]:get_available() < need_val then
				log:add( "Can't remove - missing resource" )
				return false
			end
		end
	end
	if tile.buildings.remove_fn ~= nil then
		return tile.buildings.remove_fn.check()
	end
	return #tile.buildings ~= 0
end


destroy_building.handlers[ ui.events.click ] = function( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	local building = tile.buildings[ 1 ]
	for id, item in pairs( state.factories ) do
		if item == building then
			table.remove( state.factories, id )
		end
	end
	if tile.buildings.remove_needs ~= nil then
		for id, item in pairs( tile.buildings.remove_needs ) do
			local resource, need_val = unpack( item )
			vars.resources[ resource ]:get( need_val )
		end
	end
	if tile.buildings.remove_fn ~= nil then
		tile.buildings.remove_fn.remove()
	end
	tile.buildings = {}
end

-- set_hint( destroy_building, "Demolishes local building, if there is any" )


---------------------
local build_shelter_button = ui.button:new( ui.text:new( "Shelter" ))
build_shelter_button.hint = "Builds shelter to protect from meteorites. Cost: 15 metal"

build_shelter_button.handlers[ ui.events.is_enabled ] = function( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	return #tile.buildings == 0 and vars.resources.metal:get_available() >= 15
end

build_shelter_button.handlers[ ui.events.click ] = function( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	table.insert( tile.buildings, "shelter" )
end
---------------------
local build_subsurface_mine_button = ui.button:new( ui.text:new( "Subsurface mine" ))
build_subsurface_mine_button.hint = "Builds mine for resources hidden under surface, cost: 5 metal"

build_subsurface_mine_button.handlers[ ui.events.is_enabled ] = function ()
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0 and tile.resources.subsurface.discovered and tile.resources.subsurface.resource ~= "none" and resources.metal:get_available() >= 5
end

build_subsurface_mine_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	local resource = vars.resources[ tile.resources.subsurface.resource ]
	local production = tile.resources.subsurface.richness
	local factory = Factory:new( "", {}, {{ resource, production }, }, 1, 1)
	table.insert( tile.buildings, factory )
	table.insert( state.factories, factory )
	vars.resources.metal:get( 5 )
	wait( 1 )
end
---------------------
local build_mine_button = ui.button:new( ui.text:new( "Surface mine" ))
build_mine_button.hint = "Builds mine for resources on the surface, cost: 3 metal"

build_mine_button.handlers[ ui.events.is_enabled ] = function ()
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0 and tile.resources.surface.discovered and tile.resources.surface.resource ~= "none" and resources.metal:get_available() >= 3
end

build_mine_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	local resource = vars.resources[ tile.resources.surface.resource ]
	local production = tile.resources.surface.richness
	local factory = Factory:new( "", {}, {{ resource, production }, }, 1, 1)
	table.insert( tile.buildings, factory )
	table.insert( state.factories, factory )
	vars.resources.metal:get( 3 )
	wait( 1 )
end


---------------------
local build_deep_mine_button = ui.button:new( ui.text:new( "Deep mine" ))
build_deep_mine_button.hint = "Builds mine for resources on the surface, cost: 3 metal"

build_deep_mine_button.handlers[ ui.events.is_enabled ] = function ()
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0 and tile.resources.deep.discovered and tile.resources.deep.resource ~= "none" and resources.metal:get_available() >= 3
end

build_deep_mine_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	local resource = resources[ tile.resources.deep.resource ]
	local production = tile.resources.deep.richness
	local factory = Factory:new( "", {}, {{ resource, production }, }, 1, 1)
	table.insert( tile.buildings, factory )
	table.insert( state.factories, factory )
	resources.metal:get( 3 )
	wait( 3 )
end
---------------------

local build_metal_storage_hall_button = ui.button:new( ui.text:new( "Metal storage hall" ))
build_metal_storage_hall_button.hint = "Builds storage hall for 100 metal, cost: 20 metal, 5 semiconductor"

build_metal_storage_hall_button.handlers[ ui.events.is_enabled ] = function( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0 and resources.metal:get_available() >= 20 and resources.semiconductor:get_available() >= 5
end

build_metal_storage_hall_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	table.insert( tile.buildings, "storage" )
	tile.buildings.remove_fn = {
		check = function () return resources.metal:get_max() - resources.metal:get_available() >= 100 end,
		remove = function () resources.metal:increase_max( -100) end,
	}
	resources.metal:increase_max( 100 )
	resources.metal:get( 20 )
	resources.semiconductor:get( 5 )
	wait( 3 )
end

---------------------
local build_semiconductor_storage_hall_button = ui.button:new( ui.text:new( "Semiconductor storage hall" ))
build_semiconductor_storage_hall_button.hint = "Builds storage hall for 100 semiconductor, cost: 20 metal, 5 semiconductor"

build_semiconductor_storage_hall_button.handlers[ ui.events.is_enabled ] = function( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0 and resources.metal:get_available() >= 20 and resources.semiconductor:get_available() >= 5
end

build_semiconductor_storage_hall_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	table.insert( tile.buildings, "storage" )
	tile.buildings.remove_fn = {
		check = function () return resources.semiconductor:get_max() - resources.semiconductor:get_available() >= 100 end,
		remove = function () resources.semiconductor:increase_max( -100) end,
	}
	resources.semiconductor:increase_max( 100 )
	resources.metal:get( 20 )
	resources.semiconductor:get( 5 )
	wait( 3 )
end

---------------------
local build_solar_panel = ui.button:new( ui.text:new( "Solar panel" ))
build_solar_panel.hint = "Builds solar panel to provide energy, cost: 5 semiconductor"

build_solar_panel.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0 and resources.semiconductor:get_available() >= 5
end

build_solar_panel.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		resources.semiconductor:get( 5 )
		resources.energy:add( 2 )
		local px, py = grid.player.position.x, grid.player.position.y
		local tile = grid[ grid.fn.getpos( grid, px, py )]
		table.insert( tile.buildings, "solar" )
		tile.buildings.remove_needs = {{ "energy", 2 }}
		-- how to block this field?
		-- how to remove the energy, when destroyed
	end
end

---------------------
---------------------
---------------------
---------------------

return {
	destroy_building = destroy_building,
	build_shelter_button = build_shelter_button,
	build_subsurface_mine_button = build_subsurface_mine_button,
	build_mine_button = build_mine_button,
	build_deep_mine_button = build_deep_mine_button,
	build_metal_storage_hall_button = build_metal_storage_hall_button,
	build_semiconductor_storage_hall_button = build_semiconductor_storage_hall_button,
	build_solar_panel = build_solar_panel,
}
