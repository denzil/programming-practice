local ui	= require "ui"
local class	= require "middleclass"
local grid	= require "grid"

local misc	= require "actions.misc"
local build = require "actions.build"

---------------------
local resources = vars.resources
local research_tab = vars.actions.research_tab
local build_tab = vars.actions.build_tab

---------------------
local upgrade_subsurface_search = ui.button:new( ui.text:new( "Allow subsurface search" ))
upgrade_subsurface_search.hint = "Upgrades equipment to be able search underground, costs 5 metal, 5 semiconductor"

upgrade_subsurface_search.handlers[ ui.events.is_enabled ] = function ( self )
	if not self.upgrade_done then
		if resources.metal:get_available() > 5 and resources.semiconductor:get_available() > 5 then
			return true
		end
	end
	return false
end

upgrade_subsurface_search.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	resources.metal:get( 5 )
	resources.semiconductor:get( 5 )
	research_tab:add( misc.search_subsurface_button )
	build_tab:add( build.build_subsurface_mine_button )
	self.upgrade_done = true
end

---------------------
local upgrade_deep_search = ui.button:new( ui.text:new( "Allow deep search" ))
upgrade_deep_search.hint = "Upgrades equipment to be able search deep underground, costs 15 metal, 5 semiconductor"

upgrade_deep_search.handlers[ ui.events.is_enabled ] = function ( self )
	if not self.upgrade_done then
		if resources.metal:get_available() > 15 and resources.semiconductor:get_available() > 5 then
			return true
		end
	end
	return false
end

upgrade_deep_search.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	resources.metal:get( 15 )
	resources.semiconductor:get( 5 )
	research_tab:add( misc.search_deep_button )
	--build_tab:add( build_deep_mine_button )
	self.upgrade_done = true
end

---------------------
local upgrade_iron_storage = ui.button:new( ui.text:new( "Upgrade iron storage" ))
upgrade_iron_storage.hint = "Upgrades storage to provide space for more metal, cost: 5 metal, 3 semiconductor"

upgrade_iron_storage.handlers[ ui.events.is_enabled ] = function ( self )
	if resources.metal:get_available() >= 5 and resources.semiconductor:get_available() >= 3 then
		return true
	end
	return false
end

upgrade_iron_storage.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		resources.metal:get( 5 )
		resources.semiconductor:get( 3 )
		resources.metal:increase_max( 10 )
	end
end

---------------------
local upgrade_semiconductor_storage = ui.button:new( ui.text:new( "Upgrade semiconductor storage" ))
upgrade_semiconductor_storage.hint = "Upgrades storage to provide space for more semiconductor, cost: 5 metal, 3 semiconductor"

upgrade_semiconductor_storage.handlers[ ui.events.is_enabled ] = function ( self )
	if resources.metal:get_available() >= 5 and resources.semiconductor:get_available() >= 3 then
		return true
	end
	return false
end

upgrade_semiconductor_storage.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		resources.metal:get( 5 )
		resources.semiconductor:get( 3 )
		resources.semiconductor:increase_max( 10 )
	end
end

---------------------
---------------------

return {
	upgrade_subsurface_search = upgrade_subsurface_search,
	upgrade_deep_search = upgrade_deep_search,
	upgrade_iron_storage = upgrade_iron_storage,
	upgrade_semiconductor_storage = upgrade_semiconductor_storage,
}