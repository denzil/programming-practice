local ui	= require "ui"
local class	= require "middleclass"
local grid	= require "grid"


---------------------
local resources = vars.resources

---------------------
local search_button = ui.button:new( ui.text:new( "Search area" ))
search_button.hint = "Searches nearby area for available resources"

search_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	return not tile.resources.surface.discovered
end

search_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	tile.resources.surface.discovered = true
	wait( 1 )
end

---------------------
local wait_button = ui.button:new( ui.text:new( "Wait" ))
wait_button.hint = "Wait 1s"

wait_button.handlers[ ui.events.click ] = function( self )
	if not self.cfg.hovered then
		return
	end

	wait( 1 )
end

---------------------
local search_subsurface_button = ui.button:new( ui.text:new( "Search subsurface" ))
search_subsurface_button.hint = "Probes underground for resources, takes 2s"

search_subsurface_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	return not tile.resources.subsurface.discovered
end

search_subsurface_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	tile.resources.subsurface.discovered = true
	log:add( "Subsurface resource "..tostring( tile.resources.subsurface.resource).." discovered" )
	wait( 2 )
end

---------------------
local search_deep_button = ui.button:new( ui.text:new( "Search deep ungerground" ))
search_deep_button.hint = "Probes deep underground for resources, takes 3s"

search_deep_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	return not tile.resources.deep.discovered
end

search_deep_button.handlers[ ui.events.click ] = function ( self )
	if not ( self.cfg.enabled and self.cfg.hovered ) then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	tile.resources.deep.discovered = true
	log:add( "Deep resource "..tostring( tile.resources.deep.resource ).." discovered" )
	wait( 3 )
end

---------------------
local mine_button = ui.button:new( ui.text:new( "Mine surface resources" ))
mine_button.hint = "Mines local resource, if available"

--log:add( tostring( mine_button.handlers ))
--log:add( tostring( mine_button.handlers[ ui.events.is_enabled ] ))
mine_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	local resource = tile.resources.surface.resource
	return tile.resources.surface.discovered and resources[ resource ]  and not resources[ resource ]:is_max()
end

mine_button.handlers[ ui.events.click ] = function ( self )
	self:is_hovered()
	if not self.cfg.enabled or not self.cfg.hovered then
		return
	end
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	local resource = tile.resources.surface.resource
	--log:add( resource )
	if resources[ resource ] then
		resources[ resource ]:add( 1 )
	end
	wait( 1 )
end

---------------------
---------------------
return {
	search_button = search_button,
	wait_button = wait_button,
	search_subsurface_button = search_subsurface_button,
	search_deep_button = search_deep_button,
	mine_button = mine_button,
}