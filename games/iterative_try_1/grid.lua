local class = require 'middleclass'
local player = require "player"


-------------------------------------------------------------------
-- local configuration

local grid_xsize = 120
local grid_ysize = 120

local font			= love.graphics.newFont( "font/DroidSansMono.ttf", 11 )
local font_height	= font:getHeight()
local font_width	= font:getWidth( "▬" )
local tile_side		= font_height + 1

-------------------------------------------------------------------
-- data structures

local terrain = {
--	type		= { chance, display }
	flat		= { chance = 80, display = "-", },
	mountain	= { chance = 10, display = "^", },
	valley		= { chance = 10, display = "u", },
	river		= { chance = 00, display = "~", },
}

local terrain_damage = {
	flat = { min = 0, max = 0 },
	mountain = { min = 1, max = 3 },
	valley = { min = 0, max = 1 },
	river = { min = 2, max = 4 },
}

local resource_char = {
	none	= "0",
	metal	= "M",
	semiconductor	= "S",
	water	= "W",
	uranium = "U",
}

local surface_resources = {
	-- 		= { chance, richnes { min, max }}
	--					richnes n = n resource/s
	none	= { chance = 60, richness = { 0, 0 }},
	metal	= { chance = 30, richness = { 0.1, 0.3 }},
	semiconductor	= { chance = 10, richness = { 0.1, 0.1 }},
}

local subsurface_resources = {
	none	= { chance = 40, richness = { 0, 0 }},
	metal	= { chance = 40, richness = { 0.3, 0.7 }},
	semiconductor	= { chance = 30, richness = { 0.2, 0.5 }},
	water	= { chance = 15, richness = { 0.5, 1.0 }},
	uranium	= { chance = 5, richness = { 0.1, 0.3 }},
}

local deep_resources = {
	none = { chance = 50, richness = { 0, 0 }},
	water	= { chance = 25, richness = { 0.5, 1.0 }},
	uranium	= { chance = 25, richness = { 0.3, 0.7 }},
}

map_grid = {
	xsize = grid_xsize,
	ysize = grid_ysize,
	player = player.data,
	fn = {},
}


local function new_tile ()
	local empty_tile = {
		resources = {
			surface		= {},
			subsurface	= {},
			deep		= {},
		},
		buildings 	= {},
		terrain		= 0
	}
	return empty_tile
end

-------------------------------------------------------------------
-- functions


local function getpos( grid, x, y ) 
	--log:add( "x: "..tostring( x )..", y: "..tostring( y ))
	if x > grid.xsize then x = x - grid.xsize end
	if y > grid.ysize then y = y - grid.ysize end
	if x < 1 then x = x + grid.xsize end
	if y < 1 then y = y + grid.ysize end
	--log:add( "x: "..tostring( x )..", y: "..tostring( y ))
	
	local pos = y*grid.xsize + x
	return pos
end
map_grid.fn.getpos = getpos

local function grid_iter( grid )
	local xpos = 0
	local ypos = 1
	return function ()
		xpos = xpos + 1
		if xpos > grid.xsize then
			ypos = ypos + 1
			xpos = 1
		end
		if ypos > grid.ysize then
			return nil
		end
		return getpos( grid, xpos, ypos )
	end
end

local function grid_xy_iter( grid, startx, starty, xsize, ysize )
	local startx = startx or 1
	local starty = starty or 1
	local xend = xsize+startx or grid.xsize
	local yend = ysize+starty or grid.ysize
	local xpos, ypos = startx-1, starty
	return function ()
		xpos = xpos + 1
		if xpos > xend then
			ypos = ypos + 1
			xpos = startx
		end
		if ypos > yend then
			return nil
		end
		return xpos, ypos
 	end
end


function generate_grid( grid )
	-- generate tiles
	for pos in grid_iter( grid ) do
		--io.write( tostring( pos ).."\n" )
		grid[ pos ] = new_tile()
	end
	--log:add( "grid: "..tostring( #grid ))

	-- get terrain data
	local max_chance = 0
	local last_chance = 0
	local chance_range = {}
	for my_terrain, data in pairs( terrain ) do
		max_chance = max_chance + data.chance
		table.insert( chance_range, { my_terrain, last_chance+1, max_chance })
		last_chance = max_chance
		--log:add( "terain: "..tostring( my_terrain ).." "..tostring( max_chance ))
	end
	
	-- fill grid with terrain
	for pos in grid_iter( grid ) do
		local toss = math.random( 1, max_chance )
		--log:add( "toss: "..tostring( toss ))
		grid[ pos ].terrain = terrain.flat
		for id, item in pairs( chance_range ) do
			local my_terrain, min, max = unpack( item )
			--log:add( tostring( my_terrain ).."item len: "..tostring( #item ).."min: "..tostring( min )..", max: "..tostring( max ))
			if toss >= min and toss <= max then
				---log:add( "set terrain: "..tostring( my_terrain ))
				grid[ pos ].terrain = my_terrain
				--tile.terrain = last_terrain
			end
		end
	end
	return grid
end
local grid = generate_grid( map_grid )
grid.player.position.xmax = grid.xsize
grid.player.position.ymax = grid.ysize


function add_resources( grid, layer, resources )
	-- get resource data
	local max_chance = 0
	local last_chance = 0
	local resource_range = {}
	for resource, data in pairs( resources ) do
		max_chance = max_chance + data.chance
		table.insert( resource_range, { resource, last_chance+1, max_chance })
		last_chance = max_chance
		--log:add( "terain: "..tostring( my_terrain ).." "..tostring( max_chance ))
	end
	
	-- fill grid with terrain
	for pos in grid_iter( grid ) do
		local toss = math.random( 1, max_chance )
		--log:add( "toss: "..tostring( toss ))
		local tile = grid[ pos ].resources
		tile[ layer ] = { resource = nil }
		for id, item in pairs( resource_range ) do
			local resource, min, max = unpack( item )
			if toss >= min and toss <= max then
				local res_min, res_max = unpack( resources[ resource ].richness )
				local richness = math.random( res_min*100, res_max*100 )/100
				tile[ layer ] = {}
				tile[ layer ].resource = resource
				tile[ layer ].richness = richness
				tile[ layer ].discovered = false
			end
		end
	end
	return grid
end
add_resources( grid, "surface", surface_resources )
add_resources( grid, "subsurface", subsurface_resources )
add_resources( grid, "deep", deep_resources )


function draw_grid( grid, screenx, screeny, xsize, ysize )
	local old_font = love.graphics.getFont()
	love.graphics.setFont( font )

	local pxpos, pypos = grid.player.position.x, grid.player.position.y
	if false and pxpos - xsize/2 < 1 then
		xpos = 1
	else 
		xpos = pxpos - math.floor( xsize/2 )
	end
	
	if false and pypos - ysize/2 < 1 then
		ypos = 1
	else
		ypos = pypos - math.floor( ysize/2 )
	end
	
		
	
	for x, y in grid_xy_iter( grid, xpos, ypos, xsize, ysize ) do
		-- drawing goes here
		local pos = getpos( grid, x, y )
		local tile = grid[ pos ]
		local draw = ""
		if tile.terrain~=nil then
			--log:add( "tile "..tostring( tile ))
			draw = terrain[ tile.terrain ].display
		else
			draw = "?"
		end
		
		love.graphics.setColor( 128, 128, 128 )
		love.graphics.rectangle( "line", screenx + tile_side*(x-xpos)*3, screeny + tile_side*(y-ypos)*3, tile_side*3, tile_side*3 )
		love.graphics.setColor( 255, 255, 255 )

		
		if x == grid.player.position.x and y == grid.player.position.y then
			love.graphics.print( "@", 
				screenx + tile_side*(x-xpos)*3 + font_height * 1 + 2,
				screeny + tile_side*(y-ypos)*3 + font_height * 1 + 2
			)
			love.graphics.print( tostring( grid[ pos ].resources.surface.resource ), 600, 10 )
			love.graphics.print( tostring( grid[ pos ].resources.surface.richness ), 600, 25 )
			love.graphics.print( tostring( grid[ pos ].resources.subsurface.resource ), 600, 40 )
			love.graphics.print( tostring( grid[ pos ].resources.subsurface.richness ), 600, 55 )
			love.graphics.print( tostring( grid[ pos ].resources.deep.resource ), 600, 70 )
			love.graphics.print( tostring( grid[ pos ].resources.deep.richness ), 600, 85 )
			--love.graphics.print( tostring( grid[ pos ].resources.surface.discovered ), 10, 440 )
			love.graphics.print( "Buildings: "..tostring( grid[ pos ].buildings[ 1 ]), 600, 100 )
		end
		love.graphics.print( tostring( draw ),
			screenx + tile_side*( x-xpos )*3+2, 
			screeny + tile_side*( y-ypos )*3+2
		)
		if tile.resources.surface.discovered then
				love.graphics.print( resource_char[ tile.resources.surface.resource ], 
				screenx + tile_side*(x-xpos)*3 + font_height * 2 + 2,
				screeny + tile_side*(y-ypos)*3 + font_height * 0 + 2
			)
		end
		if tile.resources.subsurface.discovered then
				love.graphics.print( resource_char[ tile.resources.subsurface.resource ], 
				screenx + tile_side*(x-xpos)*3 + font_height * 2 + 2,
				screeny + tile_side*(y-ypos)*3 + font_height * 1 + 2
			)
		end
		if tile.resources.deep.discovered then
				love.graphics.print( resource_char[ tile.resources.deep.resource ], 
				screenx + tile_side*(x-xpos)*3 + font_height * 2 + 2,
				screeny + tile_side*(y-ypos)*3 + font_height * 2 + 2
			)
		end
	end
	--love.graphics.print( tostring( #grid ), screenx, screeny-15)
	love.graphics.print( "Health status: "..tostring( grid.player.health ).."/"..tostring( grid.player.health_max ), 700, 10 )
	love.graphics.setFont( old_font )
end


function move_damage( grid )
	local x, y = player.data.position.x, player.data.position.y
	local pos = getpos( grid, x, y )
	local terrain_type = grid[ pos ].terrain
	local dmg = terrain_damage[ terrain_type ]
	player.change_health( dmg.max )
end

return map_grid