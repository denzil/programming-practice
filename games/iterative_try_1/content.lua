﻿local class 		= require "middleclass"
local factory_class = require "factory"
local ui			= require "ui"
local grid			= require "grid"

-------------------------------------------------------------------
-- setup gamestate


function base_update ( obj, dt )
	for id, item in pairs( obj ) do
		--table.insert( logger, "process: "..tostring(id) )
		if type( item ) ~= "function" then
			--table.insert( logger, "updated: "..tostring(id) )
			item:update( )
		else
			--table.insert( logger, "not processed: "..tostring(id) )
		end
	end
end

state.resources.update = base_update
state.factories.update = base_update
state.events.update = base_update

state.ui.update = function ( obj, dt ) end
state.ui.draw = function ( obj, dt )
	for id, item in ipairs( obj ) do
		item:draw()
	end
end
state.ui.event = function ( obj, evt )
	--table.insert( logger, tostring( evt ))
	for id, item in ipairs( obj ) do
		item:event( evt )
	end
end


local function ticker( self )
	--log:add( "tick"..tostring( self.items ))
	for id, item in pairs( self.items ) do
		time, updfn, finfn = unpack( item )
		--log:add( "update "..tostring( item ).."time: "..tostring( vars.time )..", waiting "..tostring( time ))
		updfn( time-vars.time )
		if time == vars.time then
			finfn()
			self.items[id] = nil
		end
	end
end

state.events.ticker = Event( function () return true end, ticker )
state.events.ticker.items = {}
titems = state.events.ticker.items
-------------------------------------------------------------------
-- support functions

wait = function( time )
	for i=1, time do
		vars.time = vars.time + 1
		love.update( 0.1 )
	end
end

-------------------------------------------------------------------
-- testing

tst_txt = ui.text:new( "test" )
tst_txt2 = ui.text:new( "test2" )
-- tst_txt:set_pos( 50, 50 )
tst_btn = ui.button:new( ui.text:new( "button" ))
tst_btn.handlers[ ui.events.click ] = function ( self, evt ) 
	if self.cfg.hovered then 
		table.insert( logger, "Clicked: "..tostring( self )) 
	end 
end

tst_bar = ui.bar:new( 1, 50 )
-- log:add( tostring( tst_bar ).." - bar1 "..tostring( tst_bar.pos ))
tst_bar2 = ui.bar:new( 10, 50 )
--tst_bar.pos = Position:new()
--tst_bar2.pos = Position:new()

--log:add( tostring( tst_bar ).." - bar1 "..tostring( tst_bar.pos ))
--log:add( tostring( tst_bar2 ).." - bar2 "..tostring( tst_bar.pos ))
--log:add( tostring( "equals "..tostring( tst_bar.pos == tst_bar2.pos )))

bar_evt = Event:new(
	function () return true end,
	function () tst_bar.status = tst_bar.status+5 end
	)
table.insert( state.events, bar_evt )

tbox = ui.hbox:new()
tbox:set_pos( 200, 400 )
tbox:add( tst_txt )
--tbox:add( tst_txt2 )
--tbox:add( tst_btn )
--tbox:add( tst_bar )
--tbox:add( tst_bar2 )
--tbox:add( tst_txt2 )
--tbox:add( tst_bar )

--local tbutton = ui.tbutton:new( "test" )
--tbox:add( tbutton )
local tabs = ui.tbox:new()
--tabs.buttons.handlers[ ui.events.item_sizer ] = same_x_sizer
--tabs:set_pos( 300, 300 )
tabs:add_tab( "Test tab 1", ui.text:new( "content1" ))
tabs:add_tab( "Test tab 2", ui.text:new( "content2" ))
tbox:add( tabs )

--table.insert( state.ui, tbox)

-------------------------------------------------------------------
-- misc ui
local hintbox = ui.vbox:new()
hintbox:set_pos( 10, 700 )

hintbox:add( ui.text:new( "Info:" ))

local hint = ui.text:new("")
hintbox:add( hint )


table.insert( state.ui, hintbox )

-------------------------------------------------------------------
-- resources
local resource_box = ui.vbox:new()
resource_box:set_pos( 10, 10 )
table.insert( state.ui, resource_box )

resource_box:add( ui.text:new( "Avaliable resources:" ))


local energy = Resource:new( "Energy", 10 )
table.insert( state.resources, enegry )
resource_box:add( energy.ui )

local rp = Resource:new( "Research points", 0 )
table.insert( state.resources, rp)
resource_box:add( rp.ui )

local metal = Resource:new( "Metal", 0, 10 )
--table.insert( state.resources, metal )
resource_box:add( metal.ui )

local semiconductor = Resource:new( "Semiconductor", 0, 10 )
resource_box:add( semiconductor.ui )

local water = Resource:new( "Water", 0, 10 )
resource_box:add( water.ui )

local oxygen = Resource:new( "Oxygen", 0, 0 )
local uranium = Resource:new( "Uranium", 0, 10 )
resource_box:add( uranium.ui )

local resources = {
	energy = energy,
	metal = metal,
	semiconductor = semiconductor,
	water = water,
	uranium = uranium,
}
vars.resources = resources

-------------------------------------------------------------------
-- item costs and productions
local costs = {
	storage				= {{ metal, 3 }, { energy, 1 }},
	-- miners
	metal_miner			= {{ metal, 5 }, { semiconductor, 2 }, { energy, 2 }},
	semiconductor_miner	= {{ metal, 5 }, { semiconductor, 2 }, { energy, 2 }},
	ice_miner 			= {{ metal, 5 }, { semiconductor, 2 }, { energy, 2 }},
	-- factories
	solar_panel_factory = {{ metal, 10 }, { semiconductor, 2 }, { energy, 4 }},
	oxygen_factory		= {{ metal, 10 }, { semiconductor, 2 }, { energy, 4 }},
	-- upgrades
	metal_miner_upgrade			= {{ semiconductor, 10 }, { rp, 5 }},
	semiconductor_miner_upgrade	= {{ semiconductor, 10 }, { rp, 5 }},
	ice_miner_upgrade			= {{ semiconductor, 10 }, { rp, 5 }},
}

local productions = {
	metal_miner			= {{ metal, 0.1 }},
	semiconductor_miner	= {{ semiconductor, 0.1 }},
	ice_miner			= {{ water, 0.1 }},
}


-------------------------------------------------------------------
-- ui extension

-- Timed button
local TButton = class( "TButton", ui.button )
function TButton:initialize( text, ctime, clickfn, finfn, enabledfn, hint_txt )
	local box = ui.vbox:new()
	box:add( ui.text:new( text ))
	local bar = ui.bar( 0, 100 )
	box:add( bar )
	ui.button.initialize( self, box )
	self.running = false
	self.clickfn = clickfn
	
	self.handlers[ ui.events.click ] = function ()
		if self.cfg.hovered and self.enabled then
			self:clickfn()
			local btn = self
			bar.status = 100
			self.running = true
			finfn = finfn or function () end
			evt = { vars.time+ctime, 
				function ( time ) bar.status = 100/ctime*(time) end ,
				function () btn.enabled = true; finfn(); bar.status = 0; self.running = false; end 
			}
			titems[ evt ] = evt
			self.enabled = false
		end
	end
	self.handlers[ ui.events.hover ] = function ()
		hint:set( hint_txt or "" )
	end
	self.handlers[ ui.events.unhover ] = function ()
		hint:set( "" )
	end
	self.handlers[ ui.events.is_enabled] = function ()
		return not self.running and enabledfn()
	end
	--log:add( "enabledfn: "..tostring( enabledfn ))
end

function create_shopbutton ( text, ctime, costs, finfn, hint_txt )
	local get_costs = ""
	if type( costs ) == "table" then
		get_costs = function () return costs end
	else
		get_costs = costs
	end
	costs = get_costs()
	
	local hint = hint_txt.." - cost: "
	for id, item in pairs( costs ) do
		local resource, cost = unpack( item )
		hint = hint..(id>1 and ", " or "")..resource.name..": "..tostring( cost )
	end
	local button = TButton:new(
		text, ctime,
		function () 
			costs = get_costs()
			for id, item in pairs( costs ) do
				local resource, cost = unpack( item )
				resource:get( cost )
			end
		end,
		finfn,
		function () 
			costs = get_costs()
			for id, item in pairs( costs ) do
				local resource, cost = unpack( item )
				if not ( resource:get_available() >= cost ) then
					return false
				end
			end
			return true
		end,
		hint
	)
	return button
end

	

-------------------------------------------------------------------
-- actions
--local action_box = ui.vbox:new()
--action_box:set_pos( 200, 10 )
--table.insert( state.ui, action_box )

local action_tbox = ui.tbox:new()
action_tbox:set_pos( 10, 300 )
--table.insert( state.ui, action_tbox )

--action_box:add( TButton:new( "test", 3 ))

--action_box:add( ui.text:new( "Avaliable actions:" ))

local research = ui.hbox:new()

local research_box = ui.vbox:new()
research_box:add( ui.text:new( "Research actions:" ))

local upgrade_box = ui.vbox:new()
upgrade_box:add( ui.text:new( "Upgrades:" ))
upgrade_box.handlers[ ui.events.item_sizer ] = same_x_sizer

local mine_box = ui.vbox:new()
mine_box.handlers[ ui.events.item_sizer ] = same_x_sizer
local manual_mine = ui.vbox:new()
manual_mine.handlers[ ui.events.item_sizer ] = same_x_sizer
manual_mine:add( ui.text:new( "Minning actions:" ))
mine_box:add( manual_mine )


local build_box = ui.hbox:new()
--build_box:add( ui.text:new( "Build actions:" ))

local build_storage = ui.vbox:new()
build_storage.handlers[ ui.events.item_sizer ] = same_x_sizer
build_storage:add( ui.text:new( "Storage:" ))
--build_box:add( build_storage )

local basic_miner = ui.vbox:new()
basic_miner.handlers[ ui.events.item_sizer ] = same_x_sizer
basic_miner:add( ui.text:new( "Automated miner:" ))
--build_box:add( basic_miner )



--action_box:add( research_box )
--action_box:add( mine_box )
--action_box:add( build_box )
--action_box:add( ui.text:new( "Other actions:" ))

action_tbox:add_tab( "Research", research )
--action_tbox:add_tab( "Minning", mine_box )
--action_tbox:add_tab( "Building", build_box )
--action_tbox:add_tab( "", _box )


--local research_btn = ui.button:new( rsrc_btn_box )
local research_btn = TButton:new( "Research area", 2, function () end, function () rp:add( 1 ); log:add( "Researched area", log.hide ) end, function () return true end, "Researches area, provides research point for upgrades, but you might discover something too" )


-------------------------------------------------------------------
-- factories
--local res_fact = Factory:new( "Research facility", {}, {{rp, 1}, }, 1)
--table.insert( state.factories, res_fact)
local fact_box = ui.vbox:new()
fact_box:set_pos( 10, 200 )

fact_box:add( ui.text:new( "Factories:" ))

--table.insert( state.ui, fact_box )

local metal_miner = Factory:new( "Metal miner", {}, productions.metal_miner, 1)
local semi_miner = Factory:new( "Semiconductor miner", {}, productions.semiconductor_miner, 1)
local ice_miner = Factory:new( "Ice miner", {}, productions.ice_miner, 1)

local solar_panel_builder = Factory:new( "Solar panel factory", {{ metal, 1 }, { semiconductor, 3 }}, {{ energy, 1 }}, 10 )


-------------------------------------------------------------------
-- in game events

local resource_adder = Event:new(
	function () return true end,
	function ()
		if rp.value >= 1 and not state.resources[ metal ] then
			state.resources[ metal ] = metal
			resource_box:add( metal.ui )
			logger:add( "You found an metal deposit" )
			action_tbox:add_tab( "Minning", mine_box )
			logger:add( "And you can build new storages now." )
			action_tbox:add_tab( "Building", build_box )
			build_box:add( build_storage )
			mine_box:add( TButton:new(
					"Mine metal", 3,
					function () end, -- onclick
					function () metal:add( 1 ); log:add( "Mined 1 metal", log.hide ) end,
					function () return not metal:is_max() end,
					"Mines area for metal"
				))
			build_storage:add( create_shopbutton(
					"Build metal storage", 5,
					function () return costs.storage end, -- cost
					--{{ metal, 3 }, { energy, 1 }}, -- cost
					function () metal:increase_max( 10 ); log:add( "Metal storage increased",log.hide ) end, -- on finish event
					"Build metal storage"
				))
		end
		if rp.value >= 5 and not state.resources[ semiconductor ] then
			state.resources[ semiconductor ] = semiconductor
			resource_box:add( semiconductor.ui )
			logger:add( "You found an semiconductor deposit" )
			logger:add( "Now you should be able to make more interesting stuff" )
			mine_box:add( TButton:new(
					"Mine semiconductor", 3,
					function () end, -- onclick
					function () semiconductor:add( 1 ); logger:add( "Mined 1 semiconductor", log.hide ) end,
					function () return not semiconductor:is_max() end,
					"Mines area for semiconductor"
				))
			build_storage:add( create_shopbutton(
					"Build semiconductor storage", 5,
					function () return costs.storage end, -- cost
					function () semiconductor:increase_max( 10 ); log:add( "Semiconductor storage increased",log.hide ) end, -- on finish event
					"Build semiconductor storage"
				))
			build_storage:add( create_shopbutton(
					"Build solar panel", 5,
					{{ metal, 1 }, { semiconductor, 3 }}, -- cost
					function () energy:add( 1 ); log:add( "Energy +1", log.hide ) end,
					"Solar panel"
				))
		end
		if rp.value >= 10 and not state.resources[ water ] then
			log:add( "You discovered underground ice." )
			log:add( "Maybe it could be useful for something?" )
			state.resources[ water ] = water
			resource_box:add( water.ui )
			mine_box:add( TButton:new(
					"Mine ice", 3,
					function () end, -- onclick
					function () water:add( 1 ); logger:add( "Mined 1 ice", log.hide ) end,
					function () return not water:is_max() end,
					"Mines ice to get water"
				))
			build_storage:add( create_shopbutton(
					"Build water storage", 5,
					function () return costs.storage end, -- cost
					function () water:increase_max( 10 ); log:add( "Water storeage increased", log.hide ) end,
					"Builds water storage, needs 3 metal, 1 energy"
				))
		end
	end
)
--table.insert( state.events, resource_adder )


local game_events = Event:new(
	function () return true end,
	function ( self )
		local time = vars.time
		if time == 1 then log:add( "Starting..." ) end
		if time == 2 then log:add( "Self-check... OK" ) end
		--if time == 2 then log:add( "" ) end
		if time == 3 then log:add( "System ready" ); research_box:add( research_btn ); research:add( research_box ) end
		if time == 40 then log:add( "Update received" ) end
		if time == 41 then log:add( "Installing... OK" ) end
		if time == 42 then log:add( "Automated mining tools now available (requires deposit)" ); build_box:add( basic_miner ) end
		if time >= 42 and state.resources[ metal ] and not self.vars.metal then
			self.vars.metal = true
			basic_miner:add( create_shopbutton(
					"Metal miner", 5, -- name, time per production
					function () return costs.metal_miner end, -- cost
					function ()
						if not self.metal_miner_built then
							self.metal_miner_built = true
							table.insert( state.factories, metal_miner )
							fact_box:add( metal_miner.ui )
							metal_miner:build( 1 )
						else
							metal_miner:build( 1 )
						end
					end,
					"Builds automatic metal miner, produces 0.1 metal/s"
				))
		end
		if time >= 42 and state.resources[ semiconductor ] and not self.vars.semiconductor then
			self.vars.semiconductor = true
			basic_miner:add( create_shopbutton(
					"Semiconductor miner", 5,
					function () return costs.semiconductor_miner end, -- cost
					function ()
						if not self.semiconductor_miner_built then
							self.semiconductor_miner_built = true
							table.insert( state.factories, semi_miner )
							fact_box:add( semi_miner.ui )
							semi_miner:build( 1 )
						else
							semi_miner:build( 1 )
						end
					end,
					"Builds automatic semiconductor miner, produces 0.1 metal/s"
				))
		end
		if time >= 42 and state.resources[ water ] and not self.vars.water then
			self.vars.water = true
			basic_miner:add( create_shopbutton(
					"Ice miner", 5,
					function () return costs.ice_miner end, -- cost
					function ()
						if not self.ice_miner_built then
							self.ice_miner_built = true
							table.insert( state.factories, ice_miner )
							fact_box:add( ice_miner.ui )
							ice_miner:build( 1 )
						else
							ice_miner:build( 1 )
						end
					end,
					"Build automatic ice miner, produces 0.1 water/s"
				))
		end
		if time >= 42 and state.resources[ semiconductor ] and not self.vars.solar_builder then
			self.vars.solar_builder = true
			basic_miner:add( create_shopbutton(
					"Solar panel factory", 5,
					function () return costs.solar_panel_factory end, -- cost
					function ()
						if not self.solar_builder_built then
							self.solar_builder_built = true
							table.insert( state.factories, solar_panel_builder )
							fact_box:add( solar_panel_builder.ui )
							solar_panel_builder:build( 1 )
						else
							solar_panel_builder:build( 1 )
						end
					end,
					"Builds solar panel factory, produces 1 energy/10 s"
				))
		end

		
		
		if time >= 60 and rp:get_available()>=15 and not self.vars.upgrades then
			self.vars.upgrades = true
			research:add( upgrade_box )
			upgrade_box:add( create_shopbutton(
					"Upgrede metal miner", 10,
					function () return costs.metal_miner_upgrade end, -- cost
					function () 
						metal_miner.produce = {{ metal_miner.produce[1][1], metal_miner.produce[1][2]+0.1, }}
					end,
					"Makes metal miners more productive"
				))
		end
		
		if self.vars.upgrades and state.resources[ semiconductor] and not self.vars.semi_miner_upgrade then
			self.vars.semi_miner_upgrade = true
			upgrade_box:add( create_shopbutton(
					"Upgrade semiconductor miners", 10,
					function () return costs.semiconductor_miner_upgrade end, -- cost
					function () 
						semi_miner.produce = {{ semi_miner.produce[1][1], semi_miner.produce[1][2]+0.1, }}
						end,
					"Makes semiconductor miners more productive"
				))
		end
			
		if self.vars.upgrades and state.resources[ water ] and not self.vars.ice_miner_upgrade then
			self.vars.ice_miner_upgrade = true
			upgrade_box:add( create_shopbutton(
					"Upgrade ice miners", 10,
					function () return costs.ice_miner_upgrade end, -- cost
					function () 
						ice_miner.produce = {{ ice_miner.produce[1][1], ice_miner.produce[1][2]+0.1, }}
						end,
					"Makes ice miners more productive"
				))
		end
			
				
		if time == 90 then log:add( "Update received()" ) end
		if time == 91 then log:add( "Installing... OK" ) end
		if time == 92 then
			log:add( "You can now produce oxygen from water" ) 
			state.resources[ oxygen ] = oxygen
			resource_box:add( oxygen.ui )
		end	
		if time == 90 then log:add( "" ) end
		--if time == 1 then log:add( "" ) end
	end
)
--table.insert( state.events, game_events )


--------------------------------------------------------------------
-- support functions

local set_hint = function( ui_element, hint_text )
	ui_element.handlers[ ui.events.hover ] = function ( self )
		hint:set( hint_text or self.hint or "" )
	end
	ui_element.handlers[ ui.events.unhover ] = function ()
		hint:set( "" )
	end
end

--------------------------------------------------------------------
-- map buttons

local actions_tab = ui.tbox:new()
actions_tab:set_pos( 10, 400 )
table.insert( state.ui, actions_tab )

local research_tab = ui.vbox:new()
local mine_tab = ui.vbox:new()
local build_tab = ui.vbox:new()
local upgrade_tab = ui.vbox:new()

actions_tab:add_tab( "Misc", research_tab ) -- renamed to "misc", as it will serve for multiple actions
--actions_tab:add_tab( "Mine", mine_tab )
actions_tab:add_tab( "Build", build_tab )
actions_tab:add_tab( "Upgrades", upgrade_tab )

vars.actions = {
	research_tab = research_tab,
	build_tab = build_tab,
	upgrade_tab = upgrade_tab,
}



---------------------
local misc = require "actions.misc"
local build = require "actions.build"
local upgrade = require "actions.upgrade"

for id, group in pairs({ misc, build, upgrade }) do
	for name, element in pairs( group ) do
		set_hint( element )
	end
end

---------------------
-- misc (research) tab
research_tab:add( misc.search_button )
research_tab:add( misc.wait_button )
research_tab:add( misc.mine_button )


---------------------
-- build tab
local build_tab_container = ui.hbox:new()
build_tab:add( build_tab_container )

local build_other_tab = ui.vbox:new()
build_other_tab:add( ui.text:new( "Other" ))
build_tab_container:add( build_other_tab )

build_other_tab:add( build.destroy_building )
build_other_tab:add( build.build_shelter_button )
build_other_tab:add( build.build_solar_panel )


local build_mine_tab = ui.vbox:new()
build_mine_tab:add( ui.text:new( "Mines" ))
build_tab_container:add( build_mine_tab )

build_mine_tab:add( build.build_mine_button )

local build_storage_tab = ui.vbox:new()
build_storage_tab:add( ui.text:new( "Storage" ))
build_tab_container:add( build_storage_tab )

build_storage_tab:add( build.build_metal_storage_hall_button )
build_storage_tab:add( build.build_semiconductor_storage_hall_button )


---------------------
-- upgrades tab
upgrade_tab:add( upgrade.upgrade_subsurface_search )
upgrade_tab:add( upgrade.upgrade_deep_search )
upgrade_tab:add( upgrade.upgrade_iron_storage )
upgrade_tab:add( upgrade.upgrade_semiconductor_storage )


---------------------
local connect_buildings = {
	landing = 1,
	habitat = 1,
	hydroponic = 1,
	oxygen = 1,
}

local get_neighbors = function( x, y )
	local neighbors = {
		{ x-1, y },
		{ x+1, y },
		{ x, y-1 },
		{ x, y+1 },
	}
	return neighbors
end
	
local get_connected = function( grid, xpos, ypos )
	local for_check	= {{ xpos, ypos }, }
	local checked 	= {[ grid.fn.getpos( grid, xpos, ypos )] = true }
	local connect_count = 0
	local connect_specific = {}
	local connect_type 	= connect_buildings[ grid[ grid.fn.getpos( grid, xpos, ypos)].buildings[ 1 ]]
	while #for_check ~= 0 do
		--log:add( "# for check: "..tostring( #for_check ))
		local px, py = unpack( for_check[ #for_check ])
		--log:add( "checking pos x: "..tostring( px )..", y: "..tostring( py ))
		for_check[ #for_check ] = nil
		local tile = grid[ grid.fn.getpos( grid, px, py )]
		local building = tile.buildings[ 1 ]
		local building_type = connect_buildings[ building ]
		--log:add( "type: "..tostring( building_type ))
		-- increase total building
		if building_type~= nil then
			if connect_type == building_type then
				--log:add( "increase count - x: "..tostring( px )..",y: "..tostring( py ))
				connect_count = connect_count + 1
				-- increase counter for specific building types
				if connect_specific[ building ] then
					connect_specific[ building ] = connect_specific[ building ] + 1
				else
					connect_specific[ building ] = 1
				end
			
				local neighbors = get_neighbors( px, py )
				for id, neighbor in ipairs( neighbors ) do
					local x, y = unpack( neighbor )
					if not checked[ grid.fn.getpos( grid, x, y )] then
						table.insert( for_check, neighbor )
						checked[ grid.fn.getpos( grid, x, y )] = true 
					end
				end
			end
		end
	end
	--[[
	log:add( "total connected building: "..tostring( connect_count ))
	for building, count in pairs( connect_specific ) do
		log:add( "count "..tostring( building )..": "..tostring( count ))
	end
	]]--
	return connect_count, connect_specific
end


---------------------
local build_habitat_button = ui.button:new( ui.text:new( "Habitat"))
build_tab:add( build_habitat_button )

build_habitat_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0
end

build_habitat_button.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		local px, py = grid.player.position.x, grid.player.position.y
		local tile = grid[ grid.fn.getpos( grid, px, py )]
		table.insert( tile.buildings, "habitat" )
		get_connected( grid, px, py )
	end
end

---------------------
local build_hydroponic_button = ui.button:new( ui.text:new( "Hydroponics"))
build_tab:add( build_hydroponic_button )

build_hydroponic_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0
end

build_hydroponic_button.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		local px, py = grid.player.position.x, grid.player.position.y
		local tile = grid[ grid.fn.getpos( grid, px, py )]
		table.insert( tile.buildings, "hydroponic" )
		get_connected( grid, px, py )
		--build_tab:remove( build_hydroponic_button )
	end
end

---------------------
local build_oxygen_button = ui.button:new( ui.text:new( "Oxygen generator"))
build_tab:add( build_oxygen_button )

build_oxygen_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and  enough metal to build mine
	return #tile.buildings == 0
end

build_oxygen_button.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		local px, py = grid.player.position.x, grid.player.position.y
		local tile = grid[ grid.fn.getpos( grid, px, py )]
		table.insert( tile.buildings, "oxygen" )
		get_connected( grid, px, py )
		--build_tab:remove( build_hydroponic_button )
	end
end

---------------------
local build_landing_pad_button = ui.button:new( ui.text:new( "Landing pad"))
build_tab:add( build_oxygen_button )

build_landing_pad_button.handlers[ ui.events.is_enabled ] = function ( self )
	local px, py = grid.player.position.x, grid.player.position.y
	local tile = grid[ grid.fn.getpos( grid, px, py )]
	-- check: nothing built here and enough metal to build mine
	return #tile.buildings == 0
end

build_landing_pad_button.handlers[ ui.events.click ] = function ( self )
	if self.cfg.enabled and self.cfg.hovered then
		local px, py = grid.player.position.x, grid.player.position.y
		local tile = grid[ grid.fn.getpos( grid, px, py )]
		table.insert( tile.buildings, "oxygen" )
		get_connected( grid, px, py )
		--build_tab:remove( build_hydroponic_button )
	end
end

