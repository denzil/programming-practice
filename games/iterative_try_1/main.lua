-- setup gamestate
state = { 
	resources = {}, 
	factories = {}, 
	events = {},
	ui = {},
}

vars = {
	time = 0,
	font = love.graphics.getFont(),
	--font_height = font:getHeight(),
}

events = {
	--[[ description:
	group = { event_name = { list of functions } }
	]]--
	tick = {}
}

-- imported support fumction
function string.split( str, sep )
end

function run_event ( event, params )
	local path = string.split( event
end


-- import 

logger 		= require "logger"
--log.level 	= log.all

local class 		= require 'middleclass'
local ui 			= require "ui"

local resource 		= require "resource"
local factory_class = require "factory"

local grid 			= require "grid"
local input			= require "input"
local player		= require "player"
local events		= require "events"
-------------------------------------------------------------------
function love.load( arg )
	if arg[#arg] == "-debug" then require("mobdebug").start() end
	ctime = 0
	mouseb = false
  
	local content = require "content"
	input.bind( 
		"escape",
		function ()
			love.event.quit()
		end
	)
	input.bind( 
		"s",
		function ()
			cwd = love.filesystem.getWorkingDirectory()
			screenshot = love.graphics.newScreenshot()
			screenshot:encode( "screen_"..os.date( "%Y-%m-%d_%H-%M-%S" )..".png" )
			log:add( "screenshot done" )
		end
	)
	input.bind({ "left", "a" }, function () player.move( -1, 0 ) move_damage( grid ) end )
	input.bind({ "right", "d"}, function () player.move( 1, 0 ) move_damage( grid ) end )
	input.bind({ "up", "w" }, function () player.move( 0, -1 ) move_damage( grid ) end )
	input.bind({ "down", "s" }, function () player.move( 0, 1 ) move_damage( grid ) end )
	input.bind( "home", function () player.move( -1, -1 ) move_damage( grid ) end )
	input.bind( "end", function () player.move( -1, 1 ) move_damage( grid ) end )
	input.bind( "pageup", function () player.move( 1, -1 ) move_damage( grid ) end )
	input.bind( "pagedown", function () player.move( 1, 1 ) move_damage( grid ) end )
end


-------------------------------------------------------------------
Event = class( "Event" )
function Event:initialize( condition, processing )
	self.condition = condition
	self.processing = processing
	self.vars = {}
	--table.insert( logger, tostring(condition) )
	--table.insert( logger, tostring(processing) )
end

function Event:update()
	if self:condition() then
		self:processing()
	end
end



-------------------------------------------------------------------
-- mouse event handling
function love.mousepressed( x, y, button)
	mouseb = true
end

function love.mousereleased( x, y, mb)
	if mouseb then
		state.ui:event( ui.events.click )
	end
	mouseb = false
end


-------------------------------------------------------------------
-- keyboard functions
function love.keypressed( key )
	input.keydown( key )
end

function love.keyreleased( key )
	input.keyup( key )
end

-------------------------------------------------------------------
function love.draw()
	font = love.graphics.getFont()
	height = font:getHeight()

	for i=1, 100 do
		font = love.graphics.getFont()
		height = font:getHeight()
		txt = logger[ #logger - 100 + i ]
		if txt then
		love.graphics.print( txt, 600, 400 + (height*(100-i)))
		end
	end
  
	state.ui.draw( state.ui )
	love.graphics.print( "Time: "..tostring( vars.time ).."s", 10, 750 )
	draw_grid( grid, 200, 10, 8, 8 )
end


------------------------------------------------------------------
function love.update(dt)
	ctime = ctime+dt
	tick_len = 0.1
	while ctime >= tick_len do
		ctime = ctime - tick_len
		--vars.time = vars.time + 1
		for id, system in pairs( state ) do
			system.update( system, dt )
		end
	end
 end
