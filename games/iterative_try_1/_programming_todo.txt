- grid - change drawing to - big squares (3x3 chars), so more information are displayed
	1 @ R B
	2 T R 
	3   R 

- factories - store mined material in place, player will havet to pick it up

- idea - resources - move adding/removing resources per turn to resource.lua -> can show change per turn

- add conditions system for tasks

- ui - make clicking smarter (= do not check element if it's not hovered)	

- ui - add current task display

- ui - rewrite - separate data from functions

- ui - allow element hiding so inactive buttons can be hidden
	- hidden element should not take any space in box

- ui - find better way to display terrain
	- buildings should be visible
	- discovered resources should be visible

- code management - separate costs and productions from code - goal - easier upgrades, cost management

- ui - size - set same size for all elements in a box
- ui - text - add text wrapping
- ui - figure out padding
- ui - box - draw size vs reported size
- ui - base - maybe change the drawing to generic (function draw_base or something) so the other classes can call it too
- ui - remove item
- ui - find better way to find why some element is greyed out


- logger - add all messages, display only configured
- logger - move to ui drawing - not in logger module, but in main
- logger - add dumping messages to disk


- far future - make the whole game event driven, so it would be possible to replay whole game by replaying event log
- far future - saving