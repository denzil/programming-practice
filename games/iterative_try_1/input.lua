-- keyboard only for now
local inputs = {}

-- function run on keydown
local function bind ( input, fn )
	-- handle multiple bindings
	if type( input ) == "table" then
		for id, control in ipairs( input ) do
			bind( control, fn )
		end
	end
	
	-- bind control to function
	local binding = {
		fn = fn,
		is_down = false,
	}
	inputs[ input ] = binding
end

local function clear( input )
	inputs[ input ] = nil
end

local function keydown ( input )
	if inputs[ input ] and not inputs[ input ].is_down then
		inputs[ input ].is_down = true
		inputs[ input ].fn()
	end
end

local function keyup ( input )
	if inputs[ input ] then
		inputs[ input ].is_down = false
	end
end

local interface = {}
interface.bind = bind
interface.clear = clear
interface.keydown = keydown
interface.keyup = keyup

return interface