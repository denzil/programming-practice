local class = require 'middleclass'
local ui = require "ui"

-------------------------------------------------------------------

Factory = class( "Factory" )
function Factory:initialize( name, consume, produce, time, amount )
	self.name = name
	self.time = time
	self.consume = consume
	self.produce = produce
	-- let's start with no factory
	self.amount = amount or 0
	self.producing = false
	self.ui = ui.text:new( self.name..": "..tostring( self.count ))
	self.factories = {}
	for i=1, self.amount do
		-- factories table format - item: -1 = not producing, positive number = start time
		table.insert( self.factories, -1 )
	end
end

function Factory:update()
	-- check finished production to make finished factories available for next round
	--log:add( "Factory update" )
	for id, factory in ipairs( self.factories ) do
		--log:add( "factory "..tostring( id ).." production check: "..tostring( factory ))
		if factory ~= -1 and factory+self.time <= vars.time then
			--log:add( "factory producing: "..tostring( id ))
			self.factories[ id ] = -1
			for pid, products in pairs( self.produce) do
				local product, upd_val = unpack( products )
				--log:add( "producing "..tostring( product.name ).." amount: "..tostring( upd_val ))
				product:add( upd_val )
			end
		end
	end
	
	-- get factories that can produce
	local available_factories = {}
	for id, factory in ipairs( self.factories ) do
		if factory == -1 then
			table.insert( available_factories, id )
			--log:add( "factory available: "..tostring( id ))
		end
	end
	
	-- run production
	for id, factory_id in ipairs( available_factories ) do
		--log:add( "can produce check for factory: "..tostring( factory_id ))
		local can_produce = true
		for id, consume in pairs( self.consume ) do
			local resource, cost = unpack( consume )
			if resource:get_available() < cost then
				--log:add( "Factory can't produce" )
				can_produce = false 
			end
		end
		if can_produce then 
			--log:add( tostring( vars.time ).." factory can produce: "..tostring( factory_id ))
			for id, consume in pairs( self.consume ) do
				local resource, cost = unpack( consume )
				resource:get( cost )
			end
			self.factories[ factory_id ] = vars.time
		end
	end
end
   
function Factory:build( amount )
	self.amount = self.amount + amount
	for i=1, amount do
		table.insert( self.factories, -1 )
	end
	self.ui:set( self.name..": "..tostring( self.amount ))
end
