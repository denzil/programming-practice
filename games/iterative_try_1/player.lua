-- player data
player = {
	position = {
		x = 1,
		y = 1,
		xmax = nil, -- default is no upper limit = nil
		ymax = nil,
	},
	health = 100,
	health_max = 100,
}

-- player functions

local function setpos ( x, y )
	player.position.x = x
	player.position.y = y
end

local function move ( xmov, ymov )
	player.position.x = player.position.x + xmov
	player.position.y = player.position.y + ymov
	
	if player.position.xmax and player.position.x > player.position.xmax then
		player.position.x = 1
	end
	if player.position.ymax and player.position.y > player.position.ymax then
		player.position.y = 1
	end
	if player.position.xmax and player.position.x < 1 then
		player.position.x = player.position.x + player.position.xmax
	end
	if player.position.ymax and player.position.y < 1 then
		player.position.y = player.position.y + player.position.ymax
	end
	--log:add( "player x: "..tostring( player.position.x )..", y: "..tostring( player.position.y ))
	vars.time = vars.time+1
end

local function change_health( amount )
	player.health = player.health - amount
	if player.health <= 0 then
		log:add( "too much damage, gameover" )
		-- gameover - not implemented yet
	end
	if player.health >= player.health_max then
		player.health = player.health_max
	end
end


-- public interface
local interface = {
	data	= player,
	setpos	= setpos,
	move	= move,
	change_health = change_health,
}

return interface
	