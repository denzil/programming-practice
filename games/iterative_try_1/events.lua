local class 		= require 'middleclass'
local grid			= require "grid"
-------------------------------------------------------------------

local Event = class( "Event" )
function Event:initialize( condition, processing )
	self.condition = condition
	self.processing = processing
	self.vars = {}
	--table.insert( logger, tostring(condition) )
	--table.insert( logger, tostring(processing) )
end

function Event:update()
	if self:condition() then
		self:processing()
	end
end

-------------------------------------------------------------------

local task_states = {
	not_yet = "not_yet",
	running = "running",
	success = "success",
	failure = "failure",
	done	= "done",
}

local tasks = {
	-- name = { state, complete_check_fn, success_fn, fail_fn }
	test = {
		state = task_states.not_yet,
		check_fn = function( self )
			self.state = task_states.failure
		end,
		success_fn = function( self )
			log:add( "success" )
			self.state = task_states.done
		end,
		fail_fn = function( self )
			log:add( "failure" )
			self.state = task_states.done
		end
	},
	meteor = {
		check_fn = function( self )
			if vars.time == 20 then
				local px, py = grid.player.position.x, grid.player.position.y
				local tile = grid[ grid.fn.getpos( grid, px, py )]
				local building = tile.buildings[ 1 ]
				if building == "shelter" then
					self.state = task_states.success
				else
					self.state = task_states.failure
				end
			end
		end,
		success_fn = function( self )
			log:add( "Waiting for meteor storm to pass...." )
			log:add( "Done, you can leave the shelter." )
			self.state = task_states.done
		end,
		fail_fn = function( self )
			log:add( "Meteor hit you, mission failed" )
			self.state = task_states.done
		end
	},
	first_resources = {
		on_start = function( self )
			log:add( "For the future buildings you will need:" )
			log:add( "150 metal, 100 semiconductor" )
		end,
		check_fn = function( self )
			if vars.resources.metal:get_available() >= 150 and vars.resources.semiconductor >= 100 then
				self.state = task_states.done
			end
		end,
		success_fn = function( self )
			log:add( "Good job, now let's put the resources into good use" )
			log:add( "Build a base consisting of: Landing pad" )
			log:add( "4x Habitat, 4x Oxygen generator, 4x Hydroponic garden" )
			log:add( "These must be connected by at least one side together" )
			tasks.build_base.state = task_states.running
		end,		
	},
	build_base = {
		check_fn = function( self )
			-- TODO: figure out how to check if buildings are done and connected
		end,
		success_fn = function( self )
		end
	},
}

local tasks_event = Event:new(
	function() return true end,
	function( self )
		--log:add( "tasks" )
		for id, event in pairs( tasks ) do
			if event.state == task_states.running then
				local result = event.check_fn( event )
			end
			if event.state == task_states.success then
				local result = event.success_fn( event )
			end
			if event.state == task_states.failure then
				local result = event.fail_fn( event )
			end
		end
	end
)
table.insert( state.events, tasks_event )

-------------------------------------------------------------------

local test_evt = Event:new(
	function () return true end,
	function () log:add( "test" ) end
)
--table.insert( state.events, test_evt )	

vars.timed_events = {
	time = -1,
	-- format: [ time ] = { message, event to run },
	[ 0 ]	= { "Landing successful, let's start exploring" },
	[ 5 ]	= { "WARNING: incoming meteor storm at t: 20s - build shelter", "meteor" },
	[ 30 ]	= { "Survived the meteorite storm. Good. Let's get some resources.", "first_resources" },
}

local time_event = Event:new(
	function() return true end,
	function( self )
		if vars.time ~= vars.timed_events.time then
			vars.timed_events.time = vars.time
			if vars.timed_events[ vars.time ] then
				local msg, task = unpack( vars.timed_events[ vars.time ])
				log:add( msg )
				if task and tasks[ task ].on_start ~= nil then
					tasks[ task ].on_start( tasks[ task ])
				end
				if task then
					tasks[ task ].state = task_states.running
				end
			end
		end
	end
)
table.insert( state.events, time_event )

-------------------------------------------------------------------
